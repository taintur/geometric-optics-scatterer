use std::env::args;

extern crate nalgebra as na;

//The polarization types: Muller matrices and Stokes vector
use na::{Matrix4};

//The geometry types:
use na::{Vector2};

pub use poroemu::subroutines::*;

fn main() {
    let input_args: Vec<String> = args().collect();
    if input_args.len() != 6 {
        println!("POROEMU: POlarizing geometRical Optics scatterEr siMUlator");
        println!("Author: Antti Mikkonen (antti.mikkonen@fmi.fi)");
        println!("");
        println!("This program computes the Muller scattering matrices of of arbitrary convex and cylinderically symmetric particles.");
        println!("Usage: poroemu [n_real] [n_imag] [size_param] [beam_amount] [angular_resolution]");
        println!("Example: poroemu 1.33 0.001 50 10000 1.0 > output.txt");
        println!("The above line also pipes the result to a text file, instead of printing it out. At least on Unix-based systems.");
        println!("");
        println!("Explanation of input parameters:");
        println!("n_real: The real part of the refraction index of the particle.");
        println!("n_imag: The imaginary part of the refractin index of the particle");
        println!("size_param: The size parameter of the ");
        println!("beam_amount: The amount of light beams simulated.");
        println!("angular_resolution: The uniform angular resolution of output in degrees.");
        println!("");
        println!("Explanation of output parameters:");
        println!("This program will print out N = (360 / angular_resolution) lines of the following format:");
        println!("[angle] [M_11] [M_12] [M_13] [M_14] [M_21] [M_22] [M_23] [M_24] [M_31] [M_32] [M_33] [M_34] [M_41] [M_42] [M_43] [M_44]");
        println!("angle: The lower bound of the angular bin. Degrees from the forward scattering direction.");
        println!("M_ij: The Muller matrix component on the i'th row and j'th column.");
        println!("");
        println!("TODO: Total internal reflection to the Muller matrices, which is needed for non-spherical particles. Edge case of hitting directly in the center of the particle.");
        return;
    }
    let n2_per_n1 : f64 = input_args[1].parse().unwrap();
    let n_imag : f64 = input_args[2].parse().unwrap();
    let size_param : f64 = input_args[3].parse().unwrap();
    let inc_beam_amt : usize = input_args[4].parse().unwrap();
    let bin_angular_resolution : f64 = input_args[5].parse().unwrap();

    let attenuation_coeff = n_imag * size_param;
    let bin_amt = ((180.0 / bin_angular_resolution) as f64).round() as usize;
    let bin_width = std::f64::consts::PI / 180.0 * bin_angular_resolution;
    let mut muller_bins: Vec<Matrix4::<f64>> = Vec::new();
    for _i in 0..bin_amt {
        muller_bins.push(Matrix4::<f64>::zeros());
    }
    let minimum_y = -1.0 + 1e-7; //the smallest y-coordinate of the scatterer
    let beam_start_x = -1.1; //where the beams start to propagate along the x-axis
    let min_intensity = 1e-12; // A beam will stop existing if its intensity is smaller than this

    for _i in 0..inc_beam_amt {
        let mut muller = Matrix4::identity();
        let beam_start_y = minimum_y * sample_start_point();

        let mut beam_position = Vector2::new(beam_start_x, beam_start_y);

        let mut beam_direction = Vector2::new(1.0, 0.0);

        let mut intersection_position = get_intersection_position(&beam_position, &beam_direction);

        let mut intersection_normal = particle_normal(&intersection_position);

        let mut inc_angle = intersection_normal.dot(& -beam_direction).acos();
        let mut refr_angle = fresnel_refr(inc_angle, n2_per_n1);
        let mut refl_beam = reflected_beam(&beam_direction, &intersection_normal);
        let mut refr_beam = refracted_beam(&beam_direction, &intersection_normal, &refr_angle);
        let mut bin_idx = find_bin_number(&refl_beam, bin_amt, &bin_width);
        muller_bins[bin_idx] = muller_bins[bin_idx] + M_reflected(inc_angle, refr_angle) * muller;
        muller = M_refracted(inc_angle, refr_angle) * muller;
        let mut intensity = muller[(0,0)];

        beam_position = intersection_position;
        beam_direction = refr_beam;
        while intensity > min_intensity {
            intersection_position = get_intersection_position(&beam_position, &beam_direction);
            intersection_normal = particle_normal(&intersection_position);
            let distance = (beam_position - intersection_position).norm();
            muller = attenuate_muller(muller, distance, attenuation_coeff);

            inc_angle = intersection_normal.dot(&beam_direction).acos();
            refr_angle = fresnel_refr(inc_angle, 1.0 / n2_per_n1);

            refl_beam = reflected_beam(&beam_direction, &intersection_normal);
            refr_beam = refracted_beam(&beam_direction, &intersection_normal, &refr_angle);

            bin_idx = find_bin_number(&refr_beam, bin_amt, &bin_width);

            muller_bins[bin_idx] = muller_bins[bin_idx] + M_refracted(inc_angle, refr_angle) * muller;
            muller = M_reflected(inc_angle, refr_angle) * muller;
            intensity = muller[(0,0)];

            beam_position = intersection_position;
            beam_direction = refl_beam;
        } //end beam_propagation loop

    } //end inc_beam loop

    //output the phase matrix
    for i in 0..bin_amt {
        let current_angle = (i + 1) as f64 * bin_angular_resolution;
        //the solid angle multiplier: this is approximately the area of the solid angle
        let avg_bin_angle = std::f64::consts::PI / 180.0 * (current_angle + current_angle - bin_angular_resolution) / 2.0;
        let muller_multiplier = 1.0 / (2.0 * avg_bin_angle.sin() * std::f64::consts::PI);
        //The resulting matrix needs to be scaled by the amount of beams.
        let muller_sum = muller_multiplier * muller_bins[i] * (1.0 / inc_beam_amt as f64);
        let print_flat = true;
        print!("{} ",180.0 - current_angle);
        print_m4(&muller_sum, print_flat);
        print!("\n");
    }
}
