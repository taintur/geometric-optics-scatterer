
pub mod subroutines {
    extern crate nalgebra as na;

    //The polarization types: Muller matrices and Stokes vector
    use na::{Matrix4};

    //The geometry types:
    use na::{Vector2};

    //For figuring out reflections and refractions
    use na::geometry::{Rotation2};

    //This is for sampling the incoming radiation.
    use rand::prelude::*;
    // ---------------------
    //  PARTICLE DEFINITION
    // ---------------------
    pub fn particle_boundary(p : &Vector2<f64>) -> f64 {
        // particle boundary is where this function returns 0
        // This function needs to be nice, i.e. on one side of the boundary it gives negatives and on
        // another positive values.
        let R: f64 = 1.0;
        // For a spherical particle of radius R this is:
        return p.norm_squared() - R.powi(2);
    }

    pub fn particle_normal(p : &Vector2<f64>) -> Vector2<f64> {
        // Finds the normal vector of the particle boundary at point p.
        // This is defined in the whole R^2
        // Essentially a normalized gradient on particle_boundary.

        // For a spherical particle this is just normalized position vector:
        return p.normalize();
    }

    // ------------------
    //  OPTICS FUNCTIONS
    // ------------------

    //M_refracted and M_reflected are from:
    //Edward Collett: Mueller-Stokes Matrix Formulation of Fresnel's Equations
    //Am. J. Phys. 39, 517 (1971); doi: 10.1119/1.1986205

    pub fn M_refracted(ang_incident : f64, ang_refracted : f64) -> Matrix4<f64> {
        let a_m = ang_incident - ang_refracted;
        let a_p = ang_incident + ang_refracted;
        let c = 0.5 * (2.0 * ang_incident).sin() * (2.0 * ang_refracted).sin() / (a_p.sin() * a_m.cos()).powi(2);
        return c * Matrix4::new(
            a_m.cos().powi(2) + 1.0, a_m.cos().powi(2) - 1.0, 0.0,             0.0,
            a_m.cos().powi(2) - 1.0, a_m.cos().powi(2) + 1.0, 0.0,             0.0,
            0.0,                     0.0,                     2.0 * a_m.cos(), 0.0,
            0.0,                     0.0,                     0.0,             2.0 * a_m.cos());
    }

    pub fn M_reflected(ang_incident : f64, ang_refracted : f64) -> Matrix4<f64> {
        let a_m = ang_incident - ang_refracted;
        let a_p = ang_incident + ang_refracted;
        let c = 0.5 * ( a_m.tan() / a_p.sin() ).powi(2);
        return c * Matrix4::new(
            a_m.cos().powi(2) + a_p.cos().powi(2), a_m.cos().powi(2) - a_p.cos().powi(2), 0.0,                          0.0,
            a_m.cos().powi(2) - a_p.cos().powi(2), a_m.cos().powi(2) + a_p.cos().powi(2), 0.0,                          0.0,
            0.0,                                   0.0,                                   -2.0 * a_p.cos() * a_m.cos(), 0.0,
            0.0,                                   0.0,                                   0.0,                          -2.0 * a_p.cos() * a_m.cos());
    }

    pub fn fresnel_refr(ang_incident : f64, n2_per_n1 : f64) -> f64 {
        let sin_ang_refr = ang_incident.sin() / n2_per_n1;
        return sin_ang_refr.asin();
    }

    pub fn reflected_beam (beam_direction : &Vector2<f64>, boundary_normal : &Vector2<f64>) -> Vector2<f64> {
        let rot = Rotation2::rotation_between(beam_direction, boundary_normal);
        let ret_dir = rot * rot * beam_direction;
        return - ret_dir;
        //The first rotation rotates the vector aligning on the boundary normal perfectly. The second
        //one rotates the beam_direction the equal amount further away.
        //This could also be just - rot * boundary_normal, since rot * beam_direction = boundary_normal,
        //but it would look counterintuitive.
    }

    pub fn refracted_beam (beam_direction : &Vector2<f64>, boundary_normal : &Vector2<f64>, refr_angle : &f64) -> Vector2<f64> {

        if beam_direction.dot(boundary_normal) > 0.0 {
            //The beam is propagating inside the particle.
            let rot = Rotation2::new(*refr_angle);
            let refr_direction = rot * boundary_normal;
            return refr_direction;
        } else {
            let rot = Rotation2::new(-refr_angle);
            let refr_direction = rot * boundary_normal;
            return - refr_direction;
        }
    }

    pub fn attenuate_muller(muller : Matrix4<f64>, distance : f64, attenuation_coeff : f64) -> Matrix4<f64> {
        return muller * (- distance * attenuation_coeff).exp();
    }
    
    // ------------------
    //  EQUATION SOLVERS
    // ------------------
    pub fn find_first_zero(f : &Fn(f64) -> f64, a : f64, b : f64) -> f64{
        // with convex particles we have at most two intersections with any beam
        // this function finds the smallest of them.

        // NOTE: This should really depend on the geometry and the initial guess.
        let sample_amount_max = 200000;
        let mut sample_amount = 200;
        while sample_amount < sample_amount_max {
            let step = (b - a) / sample_amount as f64;
            let sign_at_a = f(a).signum();
            for i in 1..sample_amount {
                let x = a + (i as f64) * step;
                if sign_at_a * f(x).signum() < 0.0 {
                    return bisect(f, x - step, x);
                }
            }
            sample_amount = 2 * sample_amount;
        }
        panic!("Couldn't find zero!");
    }

    pub fn bisect(f : &Fn(f64) -> f64, a : f64, b : f64) -> f64 {
        // bisects the interval [a,b] and finds a zero from there
        let tol_bisect = 1e-6; // The accuracy of the bisection
        let midpoint = (a + b) / 2.0;
        if b - a < tol_bisect {
            return midpoint;
        } else {
            let sign_is_positive = [f(a).signum() > 0.0,
                                    f(midpoint).signum() > 0.0,
                                    f(b).signum() > 0.0];
            return match sign_is_positive {
                [false, true, true] | [true, false, false] => bisect(f, a, midpoint),
                [true, true, false] | [false, false, true] => bisect(f, midpoint, b),
                [true, false, true] | [false, true, false] => bisect(f, (a + midpoint) / 2.0, (midpoint + b) / 2.0),
                [true, true, true] | [false, false, false] => panic!("No zero in the interval [{},{}]!",a,b),
            };
        }

    }

    pub fn get_intersection_position(beam_position : &Vector2<f64>, beam_direction : &Vector2<f64>) -> Vector2<f64> {
        let beam_trace = |t: f64| -> Vector2<f64> { t * beam_direction + beam_position };
        let boundary_intersection = |t: f64| -> f64 { particle_boundary( &beam_trace(t) ) };
        let intersection_t = find_first_zero(&boundary_intersection, 1e-5, 3.0);
        return beam_trace(intersection_t);
    }


    // -------------------------
    //  MISCELLANEOUS FUNCTIONS
    // -------------------------

    pub fn sample_start_point() -> f64 {
        let mut bad_point = true;
        let mut radius = 0.0;
        while bad_point {
            let x: f64 = random();
            let y: f64 = random();
            radius = (x*x + y*y).sqrt();
            if radius < 1.0 {
                bad_point = false;
            }
        }
        return radius;
    }

    pub fn find_bin_number(direction : &Vector2<f64>, bin_amt : usize, bin_width : &f64) -> usize {
        //bins are numbered from the negative x-axis counterclockwise
        let angle = direction.dot(&Vector2::new(-1.0, 0.0)).acos();
        for i in 0..bin_amt {
            if angle <= bin_width * (i + 1) as f64 {
                return i as usize;
            }
        }
        return (bin_amt - 1) as usize; //If the bin isn't found in the loop, the correct bin might be this, or not.
    }

    pub fn print_m4(matrix : &Matrix4<f64>, flat : bool) {
        for i in 0..4 {
            for j in 0..4 {
                print!("{} ",matrix[(i,j)]);
            }
            if !flat {
                print!("\n");
            }
        }
    }
}
