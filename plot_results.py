import matplotlib.pyplot as plt
import numpy as np

sca_data = np.genfromtxt("out.txt")

#plt.plot(sca_data[:,0],sca_data[:,1])

plt.semilogy(sca_data[:,0],sca_data[:,1])

plt.title("Phase function")
plt.ylabel("M_11")
plt.xlabel("Scattering angle")
plt.show()

plt.plot(sca_data[:,0],sca_data[:,2] / sca_data[:,1])
plt.title("Polarization degree")
plt.xlabel("Scattering angle")
plt.ylabel("-M_21 / M_11")
plt.show()
