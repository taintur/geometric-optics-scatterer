# POROEMU: POlarizing geometRical Optics scatterEr siMUlator

## Author: Antti Mikkonen (antti.mikkonen@fmi.fi)

This program computes the Muller scattering matrices of spherical particles. This can easily be expanded to arbitrary convex and cylinderically symmetric particles.

Usage: poroemu [n_real] [n_imag] [size_param] [beam_amount] [angular_resolution]

Example: poroemu 1.33 0.001 50 10000 1.0 > output.txt

The above line also pipes the result to a text file, instead of printing it out. At least on Unix-based systems.

### Explanation of input parameters:

n_real: The real part of the refraction index of the particle.

n_imag: The imaginary part of the refractin index of the particle

size_param: The size parameter of the 

beam_amount: The amount of light beams simulated.

angular_resolution: The uniform angular resolution of output in degrees.

### Explanation of output parameters:

This program will print out N = (360 / angular_resolution) lines of the following format:

[angle] [M_11] [M_12] [M_13] [M_14] [M_21] [M_22] [M_23] [M_24] [M_31] [M_32] [M_33] [M_34] [M_41] [M_42] [M_43] [M_44]

angle: The lower bound of the angular bin. Degrees from the forward scattering direction.

M_ij: The Muller matrix component on the i'th row and j'th column.